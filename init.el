;; Speed up launch by avoiding garbage collection during startup.
(setq gc-cons-threshold 20000000
      gc-cons-percentage 0.6)

;; Activate all installed packages.
(package-initialize)

;; Define my selected packages.
(setq package-selected-packages
      '(all-the-icons
        all-the-icons-dired
        auctex
        auto-complete
        company
        darktooth-theme
        doom-modeline
        doom-themes
        evil
        exec-path-from-shell
        flycheck
        geiser
        highlight-numbers
        highlight-quoted
        julia-mode
        julia-repl
        kaolin-themes
        magit
        markdown-mode
        multiple-cursors
        paredit
        polymode
        projectile
        rainbow-delimiters
        smex))

;; Add MELPA (Milkypostman's Emacs Lisp Package Archive) to the list
;; of archives.
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

;; Fetch a list of all available packages.
(when (not package-archive-contents)
  (package-refresh-contents))

;; Install selected packages.
(package-install-selected-packages)

;; Use Vi-style keybindings.
(eval-when-compile
  'evil
  (evil-mode 1)
  (define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up))

;; Treat all my themes as safe.
(setq custom-safe-themes t)

;; Load my custom theme.
(eval-when-compile
  (load-theme 'doom-one-light t))

;; Cursor color same as text.
;; (set-cursor-color "#383a42")

;; Load custom modeline.
(eval-when-compile
  (doom-modeline-mode t))

;; These settings are executed when Emacs is running in a terminal.
(unless window-system
  (xterm-mouse-mode))                   ; enable mouse usage

;; These settings are executed when Emacs is running in a GUI.
(when window-system
  (set-frame-size
   (selected-frame) 110 44)             ; set the frame (window) size
  (set-frame-font "Cascadia Code-13.0") ; set font and font-size
  (tool-bar-mode -1)                    ; disable the toolbar
  (scroll-bar-mode -1))                 ; disable the scroll bar

;; These settings are only executed when Emacs is running under macOS.
(when (eq system-type 'darwin)
  ;; Do not display the current file in the title bar.
  (setq frame-title-format nil)
  ;; Make the title bar transparent.
  (add-to-list 'default-frame-alist
               '(ns-transparent-titlebar . t))
  ;; Use the dark theme for the title- and scroll bar.
  (add-to-list 'default-frame-alist
               '(ns-appearance . light))
  ;; When opening a file from Finder, open it in an existing Emacs frame,
  ;; instead of creating a new frame.
  (setq ns-pop-up-frames nil)
  (setq mac-option-modifier nil)      ; unbind the Option key from Meta
  (setq mac-command-modifier 'meta))  ; use Command as Meta instead

;; Fix environment variables when running in a window system.
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; Set the default file encoding.
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; Wrap lines at 79 characters (in Emacs, column count starts at 0).
(setq-default fill-column 78)
(setq-default auto-fill-function 'do-auto-fill)

;; Automatically remove trailing whitespace upon save.
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Automatically make files that starts with a shebang ('#!...') executable.
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

;; When using Dired, show file sizes in a human-readable format (KB, MB, etc.).
(setq-default dired-listing-switches "-alh")

(electric-pair-mode t)			; automatically close brackets
(show-paren-mode t)	                ; highlight matching brackets
(global-hl-line-mode t)			; highlight the current line
(ac-config-default)			; auto complete code
(defalias 'yes-or-no-p 'y-or-n-p)       ; use y/n for yes and no questions
(setq inhibit-startup-screen t)	        ; don't display the startup screen
(setq initial-scratch-message nil)      ; clear the scratch buffer
(setq visible-bell t)			; do not beep
(setq column-number-mode t)	        ; show the current column number
(setq backup-inhibited t)		; disable auto-backups
(setq auto-save-default nil)		; disable auto-save
(setq-default indent-tabs-mode nil)     ; spaces instead of tabs when indenting
(setq vc-follow-symlinks t)             ; always follow symlink files
(global-auto-revert-mode t)             ; automatically load file modifications
;; Use C-v to paste, C-c to copy, C-x to cut, and C-u to undo.
(cua-mode t)

;; Pasting an item does not remove it from the clipboard.
(setq save-interprogram-paste-before-kill t)

;; If text has been selected before pasting, replace the selected text.
(delete-selection-mode)

;; Paste text at the cursor's position, not where the click is made.
(setq mouse-yank-at-point t)

;; Text completion.
(add-hook 'after-init-hook 'global-company-mode)

;;; Disable Checkdoc warnings when editing Emacs Lisp files.
(with-eval-after-load 'flycheck
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

;;; Autocomplete commands.
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;;; Old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; On-the-fly syntax checking.
(global-flycheck-mode)
(setq ispell-dictionary "american"
      ispell-local-dictionary "american")

;; Use IDO.
(with-eval-after-load 'ido
  (ido-mode t))

;; Specify the path to my interpreters for Geiser.
(defvar geiser-racket-binary "/Applications/Racket/bin/racket")
(defvar geiser-mit-binary "/usr/local/bin/scheme")

;; Use MIT Scheme as my default Geiser-interpreter
(defvar geiser-active-implementations '(mit))

(defun julia-mode-settings ()
  "Minor modes and settings for editing Julia code."
  'julia-repl
  (julia-repl-mode t))

(defun python-mode-settings ()
  "Minor modes and settings for editing Python code."
  (eval-when-compile
    (defvar python-shell-completion-native-enable nil)
    (defvar python-shell-interpreter "/Users/feli/anaconda3/bin/python3")))

(defun text-mode-settings ()
  "Minor modes for text editing."
  (display-line-numbers-mode t))

(defun prog-mode-settings ()
  "Minor modes when programming."
  (display-line-numbers-mode t)
  (highlight-numbers-mode t))

(defun lisp-settings ()
  "Minor modes for editing Lisp code."
  (paredit-mode t)
  (highlight-quoted-mode t)
  (rainbow-delimiters-mode t)
  (eldoc-mode t))

;; Highlight operators
(defface font-lock-operator-face
 '((t (:foreground "#845a84"))) "Basic face for operator." :group 'basic-faces)

;; Highlight operators for C-like languages
(dolist (mode-iter '(c-mode c++-mode))
  (font-lock-add-keywords mode-iter
   '(("\\([~^&\|!<>=,.\\+*/%-]\\)" 0 'font-lock-operator-face keep))))

;; Highlight operators for "scripting" languages
(dolist (mode-iter '(python-mode julia-mode))
  (font-lock-add-keywords mode-iter
   '(("\\([@~^&\|!<>:=,.\\+*/%-]\\)" 0 'font-lock-operator-face keep))))

;; Apply mode-specific settings as hooks.
(add-hook 'emacs-lisp-mode-hook #'lisp-settings)
(add-hook 'eval-expression-minibuffer-setup-hook #'lisp-settings)
(add-hook 'ielm-mode-hook #'lisp-settings)
(add-hook 'lisp-mode-hook #'lisp-settings)
(add-hook 'lisp-interaction-mode-hook #'lisp-settings)
(add-hook 'scheme-mode-hook #'lisp-settings)
(add-hook 'prog-mode-hook #'prog-mode-settings)
(add-hook 'python-mode-hook #'python-mode-settings)
(add-hook 'julia-mode-hook #'julia-mode-settings)

;; Reset garbage collection after load.
(setq gc-cons-threshold 16777216
      gc-cons-percentage 0.1)
