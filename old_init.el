;;; init.el --- initialize globals, load necessary packages

;;; Speed up startup by disabling garbage compilation during initialization.
(setq gc-cons-threshold 64000000)
(add-hook 'after-init-hook #'(lambda ()
                               ;; restore after startup
                               (setq gc-cons-threshold 800000)))

;;;; Packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
(when (not package-archive-contents)
    (package-refresh-contents))

(defvar my-packages
  ;; List of my packages.
  '(rainbow-delimiters
    evil
    smex
    auctex
    auto-complete
    elpy
    flycheck
    ess
    julia-repl
    markdown-mode
    paredit
    parinfer
    magit
    geiser
    slime
    polymode
    poly-R
    avy
    swiper
    counsel
    ample-theme
    github-theme
    eclipse-theme
    atom-one-dark-theme
    challenger-deep-theme
    doneburn-theme
    panda-theme
    snazzy-theme
    darktooth-theme))

(dolist (package my-packages)
  ;; Install missing packages.
  (unless (package-installed-p package)
    (package-install package)))

;;;; General settings

;; Load custom theme.
(eval-when-compile
  (load-theme 'darktooth t)
  (darktooth-modeline))

;;; vi-style bindings using Evil.
(eval-when-compile 'evil
  (evil-mode 1)
  (define-key evil-normal-state-map (kbd "s") #'avy-goto-char-2)
  (define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up)
  ;; Make C-å on a Swedish keyboard behave like C-[ on an American keyboard.
  (global-set-key (kbd "C-å") 'evil-normal-state))

;; Execute these settings when Emacs is running in a GUI.
(when window-system
  (set-frame-size
   (selected-frame) 110 44)             ; set frame (window) size
  (set-frame-font "IBM Plex Mono-13.5")   ; font and font-size
  (scroll-bar-mode -1))                 ; disable the scroll bar

;; Execute these settings when Emacs is running in a terminal.
(unless window-system
  (xterm-mouse-mode))                   ; enable terminal mouse usage

;; Execute these settings for when Emacs is running under macOS.
(when (eq system-type 'darwin)
  (setq mac-option-modifier nil)       ; unbind the Option key from Meta
  (setq mac-command-modifier 'meta))   ; use Command as Meta instead

(set-language-environment "UTF-8")      ; set default file encoding
(set-default-coding-systems 'utf-8)
(tool-bar-mode -1)                      ; disable tool bar
; (menu-bar-mode -1)                      ; disable menu bar
(defvar linum-format " %2i ")           ; set line number format
(global-hl-line-mode t)                 ; highlight current line
(electric-pair-mode t)                  ; automatically close brackets.
(defalias 'yes-or-no-p 'y-or-n-p)       ; 'y' and 'n' instead of 'yes' and 'no'.
(ac-config-default)                     ; auto complete code.

(setq inhibit-startup-screen t          ; inhibit startup screen
      visible-bell t                    ; visual bell instead of beep
      column-number-mode t              ; show current column in the mode line
      backup-inhibited t                ; disable auto-backup
      show-paren-mode t                 ; highlight brackets
      blink-matching-paren t            ; blink matching brackets
      auto-save-default nil)            ; disable auto-save

(setq-default indent-tabs-mode nil      ; spaces instead of tabs for indenting
              ;; Wrap lines at 79 characters (in Emacs, column starts at 0).
              fill-column 80
              auto-fill-function 'do-auto-fill)

;;; Delete trailing whitespace on save.
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; ;;; Auto complete commands.
;; (global-set-key (kbd "M-x") 'smex)
;; (global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; ;;; Old M-x.
;; (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;;; Org-mode

(with-eval-after-load 'org
  (define-key global-map "\C-cl" 'org-store-link)
  (define-key global-map "\C-ca" 'org-agenda)
  (defvar org-log-done t)
  ;; Define my agenda files
  (defvar org-agenda-files (list "~/org/work.org"
                               "~/org/studies.org"
                               "~/org/personal.org"))
  ;; Use actual circular bullets in lists.
  (font-lock-add-keywords 'org-mode
                          '(("^ +\\([-*]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  ;; Hide leading stars.
  (defvar org-hide-leading-stars t)
  ;; Don't fold headlines when opening an Org file.
  (defvar org-inhibit-startup-visibility-stuff t)
  ;; Display entities as symbols.
  (defvar org-pretty-entities t)
  ;; Hide emphasis markers.
  (defvar org-hide-emphasis-markers t))

(defun my-org-hook ()
  "Execute these settings when going into org-mode."
  (linum-mode 1)
  (poly-org-mode 1))

(add-hook 'org-mode-hook 'my-org-hook)

;;; Programming modes

(add-hook 'prog-mode-hook 'linum-mode)

;;; Highlight brackets according to their depth.
(with-eval-after-load 'rainbow-delimiters)

;;; Use IDO.
(with-eval-after-load 'ido
  (ido-mode t))

;;; Avy configuration.
(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-g f") 'avy-goto-line)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)

;; ;;; Python settings.
;; (add-hook 'python-mode-hook
;;           (anaconda-mode)
;;           (anaconda-eldoc-mode))

;; (with-eval-after-load 'python
;;   (setq-default python-shell-interpreter "/Users/feli/anaconda/bin/python3"))

;;; Load ESS.
(with-eval-after-load 'ess-site
  (defvar ess-ask-for-ess-directory nil))

(autoload 'r-mode "ess-site.el" "Major mode for editing R source." t)

;;; Line numbers in ESS.
(add-hook 'ess-mode-hook 'linum-mode)

;;; Load markdown-mode.
(autoload 'gfm-mode "markdown-mode.el" "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(setq auto-mode-alist (cons '("\\.text$" . gfm-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.md$" . markdown-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.markdown$" . markdown-mode) auto-mode-alist))

;;; Disable Checkdoc warnings when editing Emacs Lisp files
(with-eval-after-load 'flycheck
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

;;; Enable Flycheck.
(global-flycheck-mode)
(setq ispell-dictionary "american"
      ispell-local-dictionary "american")

;;; Show line numbers in AUXTeX.
(add-hook 'tex-mode-hook 'linum-mode)
(add-hook 'LaTeX-mode-hook 'linum-mode)
(add-hook 'bibtex-mode-hook 'linum-mode)

;;; Show line numbers in shell-script-mode.
(add-hook 'sh-mode-hook 'linum-mode)

;;; Enable Polymode.
(with-eval-after-load 'poly-R)
(with-eval-after-load 'poly-markdown)
(with-eval-after-load 'poly-org)

;;; R modes.
(add-to-list 'auto-mode-alist '("\\.md$" . poly-markdown-mode))
(add-to-list 'auto-mode-alist '("\\.Rmd$" . poly-markdown+r-mode))
(add-to-list 'auto-mode-alist '("\\.Rcpp$" . poly-r+c++-mode))
(add-to-list 'auto-mode-alist '("\\.cppR$" . poly-c++r-mode))

;; Set your lisp system and, optionally, some contribs
(setq inferior-lisp-program "/usr/local/bin/sbcl")
(setq slime-contribs '(slime-fancy))

(defun my-lisp-hook ()
  "Minor modes that should be loaded in various Lisp modes."
  ;; (paredit-mode 1)
  (parinfer-mode 1)
  (rainbow-delimiters-mode 1)
  (eldoc-mode 1))

;;; Add hooks to each Lisp mode
(add-hook 'emacs-lisp-mode-hook #'my-lisp-hook)
(add-hook 'eval-expression-minibuffer-setup-hook #'my-lisp-hook)
(add-hook 'ielm-mode-hook #'my-lisp-hook)
(add-hook 'lisp-mode-hook #'my-lisp-hook)
(add-hook 'lisp-interaction-mode-hook #'my-lisp-hook)
(add-hook 'scheme-mode-hook #'my-lisp-hook)

(custom-set-variables
 '(custom-safe-themes
   (quote
    ("36c86cb6c648b9a15d849026c90bd6a4ae76e4d482f7bcd138dedd4707ff26a5" default)))
 '(package-selected-packages
   (quote
    (snazzy-theme smex slime rainbow-delimiters poly-R parinfer paredit panda-theme magit julia-repl github-theme github-modern-theme geiser flycheck evil ess elpy eclipse-theme doneburn-theme darktooth-theme counsel challenger-deep-theme avy auto-complete auctex atom-one-dark-theme ample-theme))))
(custom-set-faces
 )

;;; init.el ends here
